import json
import os
import random
import openai
from dotenv import load_dotenv
import pandas as pd
from loguru import logger
import re

from app import database, queries
from app.logger import DEBUG
from prompt.helpers import BansPart, CommentsPart, SentimentAnalysisPart
from prompt import config

load_dotenv()
openai.api_key = os.environ.get("OPENAI_KEY")


class OpenAIChatPrompter:
    def __init__(self, language: str, self_summary: bool = False):
        self.language = language
        self.prompts = self.load_prompts_for_language(language, self_summary)

    def load_prompts_for_language(self, language: str, self_summary: bool = False) -> dict:
        if DEBUG:
            logger.info(f"Load lang: {language}")
        try:
            with open(
                    f'{os.path.dirname(__file__)}/configs/prompts.json',
                    'r', encoding="utf-8") as file:
                loaded_prompts = json.load(file)
                loaded_prompts["prompts"] = loaded_prompts["self_summary"]["prompts"] if self_summary else loaded_prompts["user_to_user_summary"]["prompts"]
                return loaded_prompts
        except FileNotFoundError:
            print(f"No prompts file found for language {language}.")
            return {}

    def get_prompt(self, number: str) -> dict:
        return self.prompts if self.prompts else {}
    
    def remove_phone_numbers(self, string):
        # Регулярное выражение для поиска телефонных номеров
        phone_pattern = r'\b\d{1,3}[-.\s]?\(?\d{3}\)?[-.\s]?\d{3}[-.\s]?\d{2}[-.\s]?\d{2}\b'
        return re.sub(phone_pattern, '', string)

    def create_chat_completion(self, number: str, insight: list | None = None, self_summary: bool = False) -> dict:
        if DEBUG:
            logger.info(f"Params: number - {number}, insight - {insight}")
        prompt = self.get_prompt(number)
        massage = ""

        # Интересные инсайты
        if insight is not None:
            massage += f"{prompt['prompts']['insights']}:\n"
            if self_summary:
                massage += "- This is my phone number.\n"
            if "bans" in insight:
                if DEBUG:
                    logger.info("Get Bans Insight")

                bans = database.fetch_data(queries.GET_BAN_QUERY.format(number=number))
                if not bans.empty:
                    massage += f"- {prompt['prompts']['total_bans']}: {bans.loc[0, 'count']}."
                else:
                    massage += f"- {prompt['prompts']['no_bans']}"

                bans_helper_df = database.fetch_data(queries.GET_BANS_INSIGHT_QUERY.format(number=number))
                if not bans_helper_df.empty:
                    massage += " " + BansPart.generate(bans_helper_df, prompt)

            if "comments" in insight:
                if DEBUG:
                    logger.info("Get Comments Insight")
                comments_helper_df = database.fetch_data(queries.GET_COMMENTS_INSIGHT_QUERY.format(number=number))
                if not comments_helper_df.empty:
                    massage += "- " + CommentsPart.generate(comments_helper_df, prompt)

            if "sentiments" in insight:
                if DEBUG:
                    logger.info("Get Sentiments Insight")
                sentiment_helper_df = database.fetch_data(queries.GET_SENTIMENT_INSIGHT_QUERY.format(number=number))
                massage += "\n"
                if not sentiment_helper_df.empty:
                    massage += "- " + SentimentAnalysisPart.generate(sentiment_helper_df, prompt)

        # Комментарии пользователей
        comment_query = queries.GET_COMMENTS_QUERY_NO_SWEARING if self_summary else queries.GET_COMMENTS_QUERY
        key = 'text' if self_summary else 'raw_text'
        comments = database.fetch_data(
            comment_query.format(number=number)
        )
        if not comments.empty:
            massage += f"\n{prompt['prompts']['comments']}\n"
            for i in range(len(comments)):
                massage += f"{str(comments.loc[i, 'created_at'])[:10]}\n {self.remove_phone_numbers(comments.loc[i, key])}\n"
        else:
            massage += prompt['prompts']['no_comments'] + "\n"
        
        
        # Возможные варианты упоминаний и количество
        names = database.fetch_data(
            queries.GET_NAMES_QUERY.format(
                table_prefix=str(number)[:5],
                number=number
            )
        )
        if not names.empty:
            massage += f"\n{prompt['prompts']['possible_mentions']}"
            for i in range(len(names)):
                massage += f"\n{names.loc[i, 'count']} {prompt['prompts']['i_mentions']} {self.remove_phone_numbers(names.loc[i, 'name'])}"
        else:
            massage += f"{prompt['prompts']['no_names']}\n"

        system = prompt['prompts']['system'][random.randint(0, len(prompt['prompts']['system']) - 1)]
        if self.language == "ru":
            system = system.replace("Neuroowl", "Нейросова")
        if DEBUG:
            logger.info(f"System prompt - {system}")
        return openai.ChatCompletion.create(
            model="gpt-4-1106-preview",
            messages=[
                {"role": "system",
                 "content": f"{system} She answers in {config.SystemPromptPostfix[self.language]}"},
                {"role": "user", "content": massage}
            ]
        ), massage
