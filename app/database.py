import os
from sqlalchemy import create_engine
from sqlalchemy import text
import pandas as pd
from dotenv import load_dotenv

load_dotenv()

SQL_USER = os.environ.get("DATABASE_USER")
SQL_PASSWORD = os.environ.get("DATABASE_PASSWORD")
SQL_DB = os.environ.get("DATABASE_NAME")
SQL_HOST = os.environ.get("DATABASE_HOST")
SQL_PORT = os.environ.get("DATABASE_PORT")
SQL_ROOT_PASSWORD = os.environ.get("DATABASE_ROOT_PASSWORD", "")
SQL_ENGINE = "mysql+mysqlconnector"

db_url = f"{SQL_ENGINE}://{SQL_USER}:{SQL_PASSWORD}" + \
         f"@{SQL_HOST}:{SQL_PORT}/{SQL_DB}"
engine = create_engine(db_url)


def fetch_data(query: str) -> pd.DataFrame:
    with engine.connect() as connection:
        result = pd.read_sql_query(text(query), connection)
    return result
