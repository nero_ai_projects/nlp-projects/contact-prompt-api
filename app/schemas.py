from pydantic import BaseModel, Field
from typing import List


class PromptRequest(BaseModel):
    number: int = Field(title="Phone Number",
                        example="123456790",
                        description="Phone Number to get prompt about.")
    lang: str = Field(title="Language",
                      example="ru",
                      description="Response language from Prompt Generator. All of lang value type, you can get from <a href=\"https://support.phrase.com/hc/en-us/articles/5818281650204-Languages-and-Locales-Strings-\">here</a>")
    insight: List[str] | None = Field(default=None,
                                      title="Insights",
                                      example=["bans", "comments", "sentiments"],
                                      description="The term **\"insight\"** in the context of your Prompt Generator API refers to specific types of information or analysis that the API can generate based on the provided data. The API can get only insights like \"bans,\" \"comments,\" and \"sentiments.\" and no more.")
    self_summary: bool = Field(default=False,
                               title="Self Summary",
                               description="The **self_summary** parameter in your Prompt Generator API seems to be a boolean flag (true/false) that dictates the context of the prompt generation based on the user's phone number.")


class PromptResponse(BaseModel):
    status: str = Field(title="Status message",
                        default="success",
                        example="success",
                        description="Status message")
    number: int = Field(title="Phone Number",
                        example="123456790",
                        description="Phone Number that have gotten prompt about.")
    neuroowl_variant_id: int = Field(title="Answer ID",
                                     default=0)
    prompt: str = Field(title="Generated prompt",
                        example="Generated prompt",
                        description="Generated prompt")
    answer: str = Field(title="Answer",
                        example="The resulting response from the generated prompt",
                        description="The resulting response from the generated prompt")
    usage_prompt_tokens: int = Field(title="Usage Prompt Tokens",
                                     default=0,
                                     description="Number Usage Prompt Tokens")
    usage_completion_tokens: int = Field(title="Usage Completion Tokens",
                                         default=0,
                                         description="Number Completion Prompt Tokens")
    created_at: str = Field(title="Created At",
                            example="2024-01-01",
                            description="Date of answer creating")    


class PromptNonAuthResponse(BaseModel):
    detail: str = Field(title="Detail",
                        default="Not authorized",
                        description="Message that you're not authorized")
