GET_BAN_QUERY = "SELECT ban_count as count FROM counters_bans WHERE `number` = {number}"
GET_NAMES_QUERY = "SELECT concat(first_name,' ', last_name) as name, count(*) as count, max(is_approved) as mx FROM contacts.contacts_{table_prefix} WHERE number_hash = crc32({number}) and is_approved >= 0 group by name_hash having mx >= 0 order by count DESC limit 30"
GET_COMMENTS_QUERY = "SELECT raw_text, created_at, profile_id FROM comments_v2 WHERE number = {number} and is_deleted = 0 and hidden_by is null order by interesting_index DESC, created_at DESC limit 30"
GET_COMMENTS_QUERY_NO_SWEARING = "SELECT text, created_at, profile_id FROM comments_v2 WHERE number = {number} and is_deleted = 0 and hidden_by is null order by interesting_index DESC, created_at DESC limit 30"
GET_BANS_INSIGHT_QUERY = "SELECT ban_count, male_bans_count, female_bans_count FROM counters_bans WHERE number = {number}"
GET_COMMENTS_INSIGHT_QUERY = """
    SELECT cls_gender, count / total_count * 100 as percent, count, total_count
    FROM (
        SELECT cls_gender,
            count(*) as count,
            (
                SELECT count(*) as total_count
                FROM comments_v2 as c
                join profiles p on c.profile_id = p.id
                WHERE number = {number}
                    and is_deleted = 0
            ) as total_count
        FROM comments_v2 as c
        join profiles p on c.profile_id = p.id
        WHERE number = {number}
        and is_deleted = 0
        and cls_gender in ('male', 'female')
        group by cls_gender
    ) a1
    order by percent DESC
"""
GET_SENTIMENT_INSIGHT_QUERY = """
    SELECT 
        male_total_items_count,
        male_negative_items_count,
        female_total_items_count,
        female_negative_items_count,
        unknown_total_items_count,
        unknown_negative_items_count
    FROM cls_numbers_sentiment
    WHERE number = {number}
"""
