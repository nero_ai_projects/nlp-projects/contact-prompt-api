import os
import json

SystemPromptPostfix = None
with open(f'{os.path.dirname(__file__)}/configs/langs.json', 'r', encoding="utf-8") as file:
    SystemPromptPostfix = json.load(file)
