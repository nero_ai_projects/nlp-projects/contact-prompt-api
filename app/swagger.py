app_description = "<div>Prompt Generator API is a versatile service designed to generate custom prompts based on user inputs.<br>"
app_description += "This service is particularly useful for applications needing dynamic, context-sensitive text generation.<br>"
app_description += "The API offers a single endpoint <b>/get_prompt</b>, which can be tailored to generate prompts based on a variety of insights.</div>"

api_key_description = "The <b>API_KEY</b> is a unique identifier required for authenticating and authorizing users of the Prompt Generator API.<br><br>"
api_key_description += "After obtaining the API keys (e.g., **API_KEY=user_1:key1;user_2:key2**) from the .env file, use one of these keys (**key1** or **key2**) for the API requests.<br>"
api_key_description += "However, before using it in the curl request, it needs to be encrypted using the SHA512 algorithm. Follow these steps:<br>"
api_key_description += "<ol><li><b>Choose an API Key</b>: Select one of the keys (e.g., key1).</li>"
api_key_description += "<li><b>Encrypt the Key Using SHA512</b>: ```bash echo -n 'key1' | openssl dgst -sha512 ```</li>"
api_key_description += "<li>Use the Encrypted Key in Requests</li></ol>"
