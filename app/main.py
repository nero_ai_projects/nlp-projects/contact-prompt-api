from fastapi import FastAPI, Depends, Request, Response, Query
from starlette.responses import JSONResponse
from dotenv import load_dotenv
from loguru import logger
from datetime import datetime

from app import schemas, database, queries, utils
from app.logger import setup_logger, generate_request_id, DEBUG
from app import swagger
from prompt import generator

load_dotenv()
setup_logger()
app = FastAPI(
    title=f"Prompt Generator API",
    version="0.0.1",
    description=swagger.app_description
)


@app.middleware("http")
async def request_middleware(request: Request, call_next) -> Response:
    with logger.contextualize(request_id=generate_request_id()):
        if DEBUG:
            logger.info("Request started")

        try:
            response = await call_next(request)
        except Exception as ex:
            if DEBUG:
                logger.exception("Request failed:")
            return Response(content=f"Request failed: {ex}", status_code=500)
        finally:
            if DEBUG:
                logger.info("Request ended")
        return response


@app.post("/get_prompt",
          tags=["API"],
          response_model=schemas.PromptResponse,
          responses={418: {"model": schemas.PromptNonAuthResponse}})
async def get_prompt(request: schemas.PromptRequest,
                     user=Depends(utils.is_authorized)) -> schemas.PromptResponse:
    try:
        if DEBUG:
            logger.info(f"Params: number - {request.number}, lang - {request.lang}, insight - {request.insight}, self_summary - {request.self_summary}")
        prompter = generator.OpenAIChatPrompter(request.lang, request.self_summary)
        response, prompt = prompter.create_chat_completion(request.number, request.insight, request.self_summary)
        answer = response["choices"][0]["message"]["content"].encode("utf-8").decode()
        return JSONResponse(
            {"status": "success",
             "number": request.number,
             "neuroowl_variant_id": 10,
             "prompt": prompt,
             "answer": answer,
             "usage_prompt_tokens": response.usage.prompt_tokens,
             "usage_completion_tokens": response.usage.completion_tokens,
             "created_at": datetime.today().strftime('%Y-%m-%d')},
                            status_code=200)
    except Exception as e:
        return JSONResponse({"status": "error", "message": str(e)},
                            status_code=500)
