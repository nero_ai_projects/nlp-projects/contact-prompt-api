import uuid
import sys
import os
from loguru import logger
from dotenv import load_dotenv

load_dotenv()
DEBUG = os.getenv("DEBUG", False)
FILE_PATH = "logs/app.log"
LOG_FORMAT = "{time:YYYY-MM-DD HH:mm:ss} - {level} - {file}:{line} - {extra[request_id]} - {message}"


def generate_request_id() -> str:
    return str(uuid.uuid4()).replace("-", "")


def setup_logger():
    global DEBUG
    if DEBUG:
        logger.add(FILE_PATH,
                format=LOG_FORMAT,
                level="INFO", rotation="00:00")
        logger.add(
            sys.stdout,
            format=LOG_FORMAT,
            level="INFO",
            colorize=True
        )
