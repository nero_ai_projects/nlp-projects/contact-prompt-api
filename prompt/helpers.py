import pandas as pd
import random


class BansPart:

    @staticmethod
    def generate(result: pd.DataFrame, prompt: dict) -> str:
        if result.empty:
            return ""

        total_bans = result['ban_count'].iloc[0]
        male_bans = result['male_bans_count'].iloc[0]
        female_bans = result['female_bans_count'].iloc[0]

        # Если банов нет или баны не посчитаны (male + female bans = 0), возвращаем пустую строку
        if total_bans <= 0 or (male_bans == 0 and female_bans == 0):
            return ""

        # Вычисляем проценты и определяем доминирующий пол
        male_percent = (male_bans / total_bans) * 100
        female_percent = (female_bans / total_bans) * 100

        dominant_gender = 'male' if male_percent > female_percent else 'female'
        dominant_percent = max(male_percent, female_percent)

        # выбор случайной фразы в зависимости от процента
        if total_bans == 1:
            phrase = random.choice(prompt["prompt_helper"]["bans_part"]["SINGLE_BAN_PHRASES"][dominant_gender])
        elif dominant_percent == 100:
            phrase = random.choice(prompt["prompt_helper"]["bans_part"]["ALL_BANS_PHRASES"][dominant_gender])
        elif dominant_percent > 60:
            phrase = random.choice(prompt["prompt_helper"]["bans_part"]["MOST_BANS_PHRASES"][dominant_gender])
        else:
            phrase = random.choice(prompt["prompt_helper"]["bans_part"]["EQUAL_BANS_PHRASES"])

        return phrase


class CommentsPart:

    @staticmethod
    def generate(result: pd.DataFrame, prompt: dict) -> str:
        # Добавление информации о комментаторах
        dominant_gender = ""
        dominant_percent = 0
        gendered_comments_count = 0
        total_comments = result['total_count'].iloc[0]

        for index, row in result.iterrows():
            if row['percent'] > dominant_percent:
                dominant_gender = row['cls_gender']
                dominant_percent = row['percent']
            gendered_comments_count += row['count']

        # Если комментариев нет или нет комментариев от мужчин или женщин, возвращаем пустую строку
        if total_comments <= 0 or gendered_comments_count <= 0:
            return ""

        # Выбор случайной фразы в зависимости от процента
        if gendered_comments_count == 1:
            phrase = random.choice(prompt["prompt_helper"]["comments_part"]["SINGLE_COMMENT_PHRASES"][dominant_gender])
        elif dominant_percent == 100:
            phrase = random.choice(prompt["prompt_helper"]["comments_part"]["ALL_COMMENTS_PHRASES"][dominant_gender])
        elif dominant_percent > 85:
            phrase = random.choice(prompt["prompt_helper"]["comments_part"]["ALMOST_ALL_COMMENTS_PHRASES"][dominant_gender])
        elif dominant_percent > 60:
            phrase = random.choice(prompt["prompt_helper"]["comments_part"]["MOST_COMMENTS_PHRASES"][dominant_gender])
        else:
            phrase = random.choice(prompt["prompt_helper"]["comments_part"]["EQUAL_COMMENTS_PHRASES"])

        # Добавление информации о количестве комментариев
        if gendered_comments_count > 1:
            phrase = f"{prompt['prompts']['total_comments']}: {total_comments}. {phrase}"

        return phrase


class SentimentAnalysisPart:

    @staticmethod
    def generate(results: pd.DataFrame, prompt: dict):
        male_total = results.loc[0, 'male_total_items_count']
        male_negative = results.loc[0, 'male_negative_items_count']
        female_total = results.loc[0, 'female_total_items_count']
        female_negative = results.loc[0, 'female_negative_items_count']
        unknown_total = results.loc[0, 'unknown_total_items_count']
        unknown_negative = results.loc[0, 'unknown_negative_items_count']

        male_positive = male_total - male_negative
        female_positive = female_total - female_negative
        unknown_positive = unknown_total - unknown_negative
        total_gendered = male_total + female_total

        male_sentiment = 'positive' if male_positive > male_negative else 'negative'
        female_sentiment = 'positive' if female_positive > female_negative else 'negative'

        response = ''

        if total_gendered / (total_gendered + unknown_total) > 0.4:
            if male_sentiment == 'positive':
                percentage_group = SentimentAnalysisPart.get_percentage_group(male_positive / male_total)
                response += random.choice(prompt["prompt_helper"]["sentiment_part"]["GENDER_SENTIMENT_PHRASES"][percentage_group]['male']['positive']) + "\n"
            else:
                percentage_group = SentimentAnalysisPart.get_percentage_group(male_negative / male_total)
                response += random.choice(prompt["prompt_helper"]["sentiment_part"]["GENDER_SENTIMENT_PHRASES"][percentage_group]['male']['negative']) + "\n"

            if female_sentiment == 'positive':
                percentage_group = SentimentAnalysisPart.get_percentage_group(female_positive / female_total)
                response += "- " + random.choice(prompt["prompt_helper"]["sentiment_part"]["GENDER_SENTIMENT_PHRASES"][percentage_group]['female']['positive']) + "\n"
            else:
                percentage_group = SentimentAnalysisPart.get_percentage_group(female_negative / female_total)
                response += "- " + random.choice(prompt["prompt_helper"]["sentiment_part"]["GENDER_SENTIMENT_PHRASES"][percentage_group]['female']['negative']) + "\n"
        else:
            total_positive = male_positive + female_positive + unknown_positive
            total_negative = male_negative + female_negative + unknown_negative
            overall_sentiment = 'positive' if total_positive > total_negative else 'negative'
            percentage_group = SentimentAnalysisPart.get_percentage_group((total_positive + total_negative) / (male_total + female_total + unknown_total))
            response += random.choice(prompt["prompt_helper"]["sentiment_part"]["OVERALL_SENTIMENT_PHRASES"][percentage_group][overall_sentiment])

        return response

    @staticmethod
    def get_percentage_group(percentage):
        if percentage >= 0.85:
            return "85"
        elif percentage >= 0.55:
            return "55"
        return "50"
