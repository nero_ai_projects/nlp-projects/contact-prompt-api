# README for Prompt Generator API

## Overview

Prompt Generator API is a versatile service designed to generate custom prompts based on user inputs. This service is particularly useful for applications needing dynamic, context-sensitive text generation. The API offers a single endpoint /get_prompt, which can be tailored to generate prompts based on a variety of insights.

Additionally, to facilitate ease of use and provide comprehensive understanding of the API's capabilities, we have included an interactive documentation powered by Swagger. This documentation is accessible at the /docs endpoint. 
```bash
http://127.0.0.1/docs
```
The Swagger UI offers a user-friendly interface to interact with the API, allowing users to explore its features, test the endpoint in real-time, and view request and response examples. This interactive documentation is an invaluable tool for developers to quickly get up to speed with the API's functionalities and integrate them into their applications efficiently.

## Features

- Single API Endpoint: The API provides a /get_prompt endpoint for generating prompts.
- Customizable Inputs: Users can specify different parameters like phone number and language.
- Support for Multiple Languages: The API supports a range of languages, including German (de), English (en), Spanish (es), French (fr), Italian (it), Dutch (nl), Portuguese (pt), Russian (ru), Turkish (tr), and Ukrainian (uk).
- Insight-Based Prompt Generation: Users can specify insights like "bans," "comments," and "sentiments" to tailor the generated prompts.

## Usage

### Endpoint: /get_prompt

This endpoint generates a prompt based on the provided parameters.

Making a Request

1. **With Insights and Self Summary:**
    ```bash
    curl -X 'POST' \
    'http://127.0.0.1/get_prompt' \
    -H 'accept: application/json' \
    -H 'api_key: YOUR_ENCRYPTED_API_KEY' \
    -H 'Content-Type: application/json' \
    -d '{
        "number": phone_number,
        "lang": lang,
        "insight": ["bans", "comments", "sentiments"],
        "self_summary": true/false
    }'

    ```

    Note: Replace phone_number and lang with actual values. The insight array can have one or multiple insights.

2. **Without Insights (with or without Self Summary):**

    ```bash
    curl -X 'POST' \
    'http://127.0.0.1/get_prompt' \
    -H 'accept: application/json' \
    -H 'api_key: YOUR_ENCRYPTED_API_KEY' \
    -H 'Content-Type: application/json' \
    -d '{
        "number": phone_number,
        "lang": lang,
        "self_summary": true/false
    }'

    ```

## Body Params Description

1. The **self_summary** parameter in your Prompt Generator API seems to be a boolean flag (true/false) that dictates the context of the prompt generation based on the user's phone number. Here's a breakdown of its functionality: The self_summary parameter accepts a boolean value – **true** or **false**.

2. The term **"insight"** in the context of your Prompt Generator API refers to specific types of information or analysis that the API can generate based on the provided data. Here's a detailed explanation of what insights might mean in this context and how they could be used. Types of Insights: The API can get only insights like "bans," "comments," and "sentiments." and no more.
    
    - Bans: This could refer to identifying or summarizing instances where a user or content has been banned or flagged for violating guidelines.

    - Comments: This might involve analyzing user-generated comments for trends, topics, sentiment, or other notable features.

    - Sentiments: This typically involves sentiment analysis, which determines the emotional tone behind a text. This could be used to understand user opinions, attitudes, and emotions.

3. The **lang** parameter in your Prompt Generator API refers to the language in which the prompt should be generated. All of lang value type, you can get from https://support.phrase.com/hc/en-us/articles/5818281650204-Languages-and-Locales-Strings-

    - Bans: This could refer to identifying or summarizing instances where a user or content has been banned or flagged for violating guidelines.

## API Key Encryption Instructions
After obtaining the API keys (e.g., **API_KEY=user_1:key1;user_2:key2**) from the .env file, use one of these keys (**key1** or **key2**) for the API requests. However, before using it in the curl request, it needs to be encrypted using the SHA512 algorithm. Follow these steps:

1. **Choose an API Key**: From your .env file, select one of the keys (e.g., key1).
2. **Encrypt the Key Using SHA512**: Encrypt the chosen key using the SHA512 algorithm. This can be done using command-line tools or online encryption services. For example, using the command line:
    ```bash
    echo -n "key1" | openssl dgst -sha512
    ```
3. Use the Encrypted Key in Requests: Replace **YOUR_API_KEY** in the curl request with your SHA512 encrypted key. For example:
    ```bash
    curl -X 'POST' \
    'http://127.0.0.1/get_prompt' \
    -H 'accept: application/json' \
    -H 'api_key: [ENCRYPTED_API_KEY]' \
    -H 'Content-Type: application/json' \
    -d '{
        "number": phone_number,
        "lang": lang,
        "insight": ["bans", "comments", "sentiments"],
        "self_summary": true/false
    }'
    ```

## Response Structure

The API response is structured as follows:

```bash
{
  "status": "success/error",
  "number": number,
  "neuroowl_variant_id": neuroowl_variant_id,
  "prompt": "Prompt",
  "answer": "Answer",
  "usage_prompt_tokens": count_prompt_tokens,
  "usage_completion_tokens": count_completion_tokens,
  "created_at": "some_date"
}
```

## .env File

Create a .env file in the root of your project with the following structure:

```.env
OPENAI_KEY=val
DATABASE_HOST=val
DATABASE_ROOT_PASSWORD=val
DATABASE_NAME=val
DATABASE_USER=val
DATABASE_PASSWORD=val
DATABASE_PORT=val
DEBUG=val
API_KEY=user_1:key1;user_2:key2

```
Note: Replace val with actual values.


## API Key Parsing

The API keys are parsed and managed as follows:

```python
# Parsing API Keys from .env file
API_KEYS_DICT = None

def set_up_api_keys():
    """
    Load and parse API KEY from .env file.
    :return: A dictionary of api_keys if API KEY exists, else an empty dictionary.
    """
    api_key = os.getenv('API_KEY')
    api_keys_dict = {}
    if not api_key:
        return api_keys_dict
    list_keys = api_key.split(";")
    for item in list_keys:
        temp_list = item.split(":")
        api_keys_dict[temp_list[0]] = temp_list[1]
    return api_keys_dict

async def is_authorized(api_key: str | None = Header(title="api_key", convert_underscores=False)) -> str:
    """
    Check authorization using the provided api_key.
    :param api_key: The hash used for authorization.
    :return: (True, user) if authorized, else (False, None).
    """
    if not api_key:
        raise HTTPException(status_code=418, detail="Not authorized")

    global API_KEYS_DICT
    if API_KEYS_DICT is None:
        API_KEYS_DICT = set_up_api_keys()

    for user in API_KEYS_DICT:
        expected_hash = hashlib.sha512(API_KEYS_DICT[user].encode()).hexdigest()

        if api_key == expected_hash:
            return user

    raise HTTPException(status_code=418, detail="Not authorized")

```

## Conclusion
Prompt Generator API is a flexible and easy-to-use service for generating customized prompts. By supporting multiple languages and insights, it caters to a wide range of applications and use cases.
