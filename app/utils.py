import os
import hashlib
from fastapi import Header, HTTPException
import uuid

from app import swagger

API_KEYS_DICT = None
generate_request_id = lambda: str(uuid.uuid4()).replace("-", "")


def set_up_api_keys():
    """
    Load API KEY from env file and parse it

    :return: dict of api_keys if API KEY exist else empty dict
    """
    api_key = os.getenv('API_KEY')
    api_keys_dict = {}
    if not api_key:
        return api_keys_dict
    list_keys = api_key.split(";")
    for item in list_keys:
        temp_list = item.split(":")
        api_keys_dict[temp_list[0]] = temp_list[1]
    return api_keys_dict


async def is_authorized(api_key: str | None = Header(title="api_key",
                                                     convert_underscores=False,
                                                     description=swagger.api_key_description)) -> str:
    """
    Check authorization by api_key
    :param api_key: hash that will be used to authorization
    :return: (True, user) if authorized else (False, None)
    """
    if not api_key:
        raise HTTPException(status_code=418, detail="Not authorized")

    global API_KEYS_DICT
    if API_KEYS_DICT is None:
        API_KEYS_DICT = set_up_api_keys()

    for user in API_KEYS_DICT:
        expected_hash = hashlib.sha512(
            API_KEYS_DICT[user].encode()
        ).hexdigest()

        if api_key == expected_hash:
            return user

    raise HTTPException(status_code=418, detail="Not authorized")
